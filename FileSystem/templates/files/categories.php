<?php require_once '../../php/auth-page-controller.php'; ?>
<input type="hidden" id="auth" name="auth" value="<?php echo $auth; ?>">
<div class="uk-child-width-expand@s uk-margin-small-top uk-flex-top" uk-grid>
    <div>

        <div class="uk-flex uk-flex-wrap ">
            <div class="uk-width-1-1">
                <h2 class="uk-h3"style="font-family: Gotham; font-size: 25px; color:313131">File Category Manager</h2>
            </div>
            <div class="uk-width-1-1">
                <ul class="uk-breadcrumb">
                    <li class="uk-disabled"><a href="#"style="font-size: 12px;">Home</a></li>
                    <li class="uk-disabled"><a href="#files/manage"style="font-size: 12px;">Files</a></li>
                    <li class="uk-disabled"><a href="#"style="font-size: 12px;">Categories</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div>

    </div>
</div>
<hr class="uk-divider-icon">
<div class="uk-child-width-expand@s" uk-grid>
    <div>

        <form class="uk-form-stacked uk-panel" method="POST">
            <label style="font-size: 12px;">Add Category</label>

            <div class="uk-margin" >
                <label class="uk-form-label" for="form-stacked-text" style="font-size: 12px;" >File Category Name</label>
                <div class="uk-form-controls">
                    <input type="hidden" id="id" value="0" name="id" >
                    <input class="uk-input" id="form-stacked-text" type="text" placeholder="e.g(CategoryName(Semester_SY))" name="category" required style="border-radius: 10px;">
                </div>
            </div>
            <div class="uk-margin">
                <textarea class="uk-textarea" rows="2" placeholder="Description" name="description" id="description"></textarea>
            </div>
            <div class="uk-margin">
                <p uk-margin>
                    <input type="submit" class="uk-button uk-button-primary" value= "Create Category" name="add_category_submit" style = "border-radius: 10px; font-size: 12px;">
                </p>
            </div>
        </form>
    </div>

    <div>

        <label style="font-size: 12px;">Categories</label>

            <div class="uk-panel uk-panel-scrollable" style="height: 300px;border-radius: 10px;">
                <?php include_once '../../php/fetch-category-controller.php'; ?>
            </div>
    </div>
</div>
<script>
    var auth = $('#auth').val();
    if(auth == "restricted") {
        window.location.replace("index.php#restricted/page");
    }
</script>
<script>
    $("button").on('click', function() {
        var id = $(this).attr("data-id");
        var type = $(this).attr("data-type");
        var tr = $(this).closest('tr');

        if (type == "edit") {
            $.ajax({
                url: "php/select-category-controller.php?id=" + id,
                dataType: 'json',
                success: function(result) {
                    $('#form-stacked-text').val(result.name);
                    $('textarea#description').val(result.description);
                    $('#id').val(result.id);
                }
            });
        } else if (type == "delete") {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: "php/delete-category-controller.php?id=" + id,
                        success: function(result) {
                            if(result == "success") {
                                tr.fadeOut(1000, function() {
                                $(this).remove();
                                });
                                Swal.fire(
                                    'Deleted!',
                                    'Your file has been deleted.',
                                    'success'
                                )
                            }else if($result == "fail") {
                                Swal.fire(
                                  'Unsuccessful Deletion!',
                                  'Consult Admin',
                                  'error'
                                )
                            }
                        }
                    });
                }
            })
        } 
    });
</script>