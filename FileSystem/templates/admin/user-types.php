<?php require_once '../../php/auth-page-controller.php'; ?>
<input type="hidden" id="auth" name="auth" value="<?php echo $auth; ?>">
<div class="uk-child-width-expand@s uk-margin-small-top uk-flex-top" uk-grid>
    <div>

        <div class="uk-flex uk-flex-wrap ">
            <div class="uk-width-1-1">
                <h2 class="uk-h3" style="font-family: Gotham; font-size: 25px; color:313131">Add User Types</h2>
            </div>
            <div class="uk-width-1-1">
                <ul class="uk-breadcrumb">
                    <li class="uk-disabled"><a href="#" style="font-size: 12px;">Home</a></li>
                    <li class="uk-disabled"><a href="#admin/manage" style="font-size: 12px;">Users</a></li>
                    <li class="uk-disabled"><a href="#" style="font-size: 12px;">Types</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<hr class="uk-divider-icon">
<div class="uk-child-width-expand@s" uk-grid>
    <div>

        <form class="uk-form-stacked uk-panel" method="POST">
            <label style="font-size: 12px;">Add Role</label>

            <div class="uk-margin">
                <label class="uk-form-label" for="user-type" style="font-size: 12px;">User Type</label>
                <div class="uk-form-controls">
                    <input type="hidden" id="uid" value="0" name="uid">
                    <input class="uk-input" id="user-type" type="text" placeholder="e.g(Librarian)" name="user-type" required style="border-radius: 10px;font-size: 12px;">
                </div>
            </div>
            <div class="uk-margin">
                <p uk-margin>
                    <input type="submit" class="uk-button uk-button-primary" value="Submit" name="add_user_type_submit" style="border-radius: 10px;font-size: 12px;">
                </p>
            </div>
        </form>
    </div>

    <div>
        <label style="font-size: 12px;">Categories</label>

            <div class="uk-panel uk-panel-scrollable" style="height: 300px;border-radius: 10px;">
                <?php include_once '../../php/fetch-utype-controller.php'; ?>
        </div>
    </div>
</div>
<script>
    var auth = $('#auth').val();
    if(auth == "restricted") {
        window.location.replace("index.php#restricted/page");
    }
</script>
<script>
    $("button").on('click', function() {
        var id = $(this).attr("data-id");
        var type = $(this).attr("data-type");
        var tr = $(this).closest('tr');
    
        if (type == "edit-utype") {
            $.ajax({
                url: "php/select-utype-controller.php?id=" + id,
                dataType: 'json',
                success: function(result) {
                    $('#user-type').val(result.description);
                    $('#uid').val(result.id);
                }
            });
        } else if (type == "delete-utype") {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: "php/delete-utype-controller.php?id=" + id,
                        success: function(result) {
                            if(result == "success") {
                                tr.fadeOut(1000, function() {
                                $(this).remove();
                                });
                                Swal.fire(
                                    'Deleted!',
                                    'Role has been deleted.',
                                    'success'
                                )
                            }else {
                                Swal.fire(
                                  'Unsuccessful Deletion!',
                                  'Consult Admin',
                                  'error'
                                )
                            }
                        }
                    });
                    
                }
            })
        } 
    });
</script>